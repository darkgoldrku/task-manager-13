package ru.t1.bugakov.tm.api;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
